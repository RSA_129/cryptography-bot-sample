from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Cipher import PKCS1_OAEP
from config import delimiter_for_enc_text


ln = 40


def gen_keys():
    random_generator = Random.new().read
    key = RSA.generate(1024, random_generator)
    private_key, public_key = key.export_key().decode(), key.publickey().export_key().decode()
    return private_key, public_key


def encode_text(public_key, plain_text):
    cipher = PKCS1_OAEP.new(RSA.importKey(public_key))
    encrypted_text = []

    for i in range(0, len(plain_text), ln):
        txt = plain_text[i:i + ln].encode()
        encrypted_text.append(cipher.encrypt(txt))
    return delimiter_for_enc_text.join(encrypted_text)


def decode_text(private_key, encrypted_text):
    cipher = PKCS1_OAEP.new(RSA.importKey(private_key))
    encrypted_text = encrypted_text.split(delimiter_for_enc_text)
    decrypted_text = []
    for i in range(0, len(encrypted_text)):
        txt = cipher.decrypt(encrypted_text[i]).decode()
        decrypted_text.append(txt)
    return ''.join(decrypted_text)
