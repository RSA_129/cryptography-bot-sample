from aiogram.utils import executor

from create_bot import dp

from handlers_pack import main_handler
main_handler.register_client(dp)

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)


