import asyncio
import binascii

from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

from aiogram.types import ParseMode, ReplyKeyboardRemove
from message_handler import send_message
from kb import kbs, config_buts
from config import Commands
from en_decrypting import rsa
import base64


class FSM_main(StatesGroup):
    method = State()
    get_key = State()
    get_text = State()

    keys_method = State()


async def comm_start(message: types.Message, state: FSMContext):
    if await state.get_state():
        await state.finish()
    txt = f'''
What do you want to do?
/{Commands.encode} - encode text
/{Commands.decode} - decode text
/{Commands.get_keys} - generate keys
'''
    await send_message(message.from_user.id, txt)


async def choose_tpe(mes: types.Message, state: FSMContext):
    if await state.get_state():
        await state.reset_state()
    if mes.get_command(pure=True) == Commands.encode:
        txt = 'Choose method of encoding'
    else:
        txt = 'Choose method of decoding'

    async with state.proxy() as da:
        da['tpe'] = mes.get_command(pure=True)
    await send_message(mes.from_user.id, txt, await kbs.get_buts(config_buts.method_encode))
    await FSM_main.method.set()


async def choose_method(mes: types.Message, state: FSMContext):
    if mes.text in config_buts.method_encode:
        async with state.proxy() as da:
            da['method'] = mes.text
        if da['tpe'] == Commands.encode:
            txt = "Now type public key to encode message " \
                  ".\nIf you haven't generated it here's " \
                  f" command /{Commands.get_keys}"
        else:
            txt = "Now type private key to decode message " \
                  ".\nIf you haven't generated it here's " \
                  f"command /{Commands.get_keys}"
        await send_message(mes.from_user.id, txt, ReplyKeyboardRemove())
        await FSM_main.get_key.set()
    else:
        await send_message(mes.from_user.id, 'Wrong choice, repeat!')


async def get_key(mes: types.Message, state: FSMContext):
    async with state.proxy() as da:
        da['key'] = mes.text
    if da['tpe'] == Commands.encode:
        txt = 'Now type text to encode (maximum is approximately 850 symbols)'
    else:
        txt = 'Now type text to decode'
    await send_message(mes.from_user.id, txt)
    await FSM_main.get_text.set()


async def get_text(mes: types.Message, state: FSMContext):
    too_long = False
    async with state.proxy() as da:
        da['mes'] = mes.text
    try:
        if da['tpe'] == Commands.encode:
            txt = await get_encoded_text(state)
            if len(txt) > 4000:
                too_long = True
                txt = "Too long message.\nType shorter"
            else:
                await send_message(mes.from_user.id, "Here's encoded message:")
        else:
            txt = await get_decoded_text(state)
            await send_message(mes.from_user.id, "Here's decoded message:")

        await send_message(mes.from_user.id, txt, ReplyKeyboardRemove())
    except (binascii.Error, ValueError) as e:
        print(e)
        await send_message(mes.from_user.id, f"Wrong key or encoded text!\nTry again\n/{da['tpe']}")
    if not too_long:
        await state.finish()


async def get_encoded_text(state: FSMContext):
    txt = 'ERROR'
    async with state.proxy() as da:
        key = base64.urlsafe_b64decode(da['key'].encode()).decode()
        if da['method'] == 'RSA':
            txt = rsa.encode_text(key, da['mes'])
    txt = base64.urlsafe_b64encode(txt).decode()
    return txt


async def get_decoded_text(state: FSMContext):
    txt = 'ERROR'
    async with state.proxy() as da:
        key = base64.urlsafe_b64decode(da['key'].encode()).decode()
        mes = base64.urlsafe_b64decode(da['mes'].encode())
        if da['method'] == 'RSA':
            txt = rsa.decode_text(key, mes)
    return txt


# KEYS
async def tpe_of_keys(mes: types.Message, state: FSMContext):
    if await state.get_state():
        await state.finish()
    await send_message(mes.from_user.id, 'Choose type of key', await kbs.get_buts(config_buts.method_keys))
    await FSM_main.keys_method.set()


async def make_keys(mes: types.Message, state: FSMContext):
    if mes.text in config_buts.method_keys:
        if mes.text == 'RSA':
            keys = rsa.gen_keys()
            await send_message(mes.from_user.id, "Here's public key:", ReplyKeyboardRemove())
            await send_message(mes.from_user.id, base64.urlsafe_b64encode(keys[1].encode()).decode())
            await asyncio.sleep(0.25)
            await send_message(mes.from_user.id, "Watch out, private key. Exposing it may "
                                                 "result in decoding private message:")
            priv_key = base64.urlsafe_b64encode(keys[0].encode()).decode()
            priv_key = f"<tg-spoiler>{priv_key}</tg-spoiler>"
            await send_message(mes.from_user.id, priv_key, parse_mode=ParseMode.HTML)
            await state.finish()
    else:
        await send_message(mes.from_user.id, 'Wrong choice, repeat!')



def register_client(dp: Dispatcher):
    dp.register_message_handler(comm_start, commands=['start'], state='*')
    dp.register_message_handler(choose_tpe, commands=[Commands.encode, Commands.decode], state='*')
    dp.register_message_handler(tpe_of_keys, commands=[Commands.get_keys], state='*')
    dp.register_message_handler(choose_method, state=[FSM_main.method])
    dp.register_message_handler(get_key, state=[FSM_main.get_key])
    dp.register_message_handler(get_text, state=[FSM_main.get_text])

    dp.register_message_handler(make_keys, state=[FSM_main.keys_method])
