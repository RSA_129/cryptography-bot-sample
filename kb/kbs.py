from aiogram.types import ReplyKeyboardMarkup, KeyboardButton


async def get_buts(ray):
    menu = ReplyKeyboardMarkup(resize_keyboard=True)
    for i in ray:
        menu.insert(KeyboardButton(i))
    return menu

